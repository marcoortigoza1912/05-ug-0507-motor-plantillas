
const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');
const { update, destroy } = require('./integrantes.controller');

const tipoMediaController = {
    index: async function(req, res){
        const tipomedia = await getALL(`SELECT * FROM tipomedia where borralog ='FALSE' order by orden asc`);
        if (!tipomedia) {
            response.status(404).render("error");
        } else {
            res.render("admin/tipo_media/index",{
                tipomedia: tipomedia,
            });
        }
    },
    create: async function(req,res){
        const mensaje = req.query.mensaje;
        res.render('admin/tipo_media/formularioTipoMedia', {
        mensaje: mensaje
    });
    },
    
    
    
    store: async function(req, res) {
        const { nombre, borralog } = req.body;
    
        // Verificar si los campos obligatorios están presentes y no están vacíos
        if (!nombre || !borralog || nombre.trim() === '' || borralog.trim() === '') {
            return res.redirect(`/admin/tipo_media/crear?mensaje="Todos los campos son obligatorios. Favor verificar"&nombre=${nombre}&borralog=${borralog}`);
        }
    
        // Consultar si ya existe un registro con el mismo nombre
        db.get("SELECT * FROM tipomedia WHERE nombre = ?", [nombre], (err, row) => {
            if (err) {
                console.error("Error al consultar en la base de datos:", err);
                return res.sendStatus(500);
            }
            if (row) {
                // Si ya existe un registro con el mismo nombre, redirigir con un mensaje de error
                return res.redirect(`/admin/tipo_media/crear?mensaje="El nombre '${nombre}' ya existe. Favor elegir otro"&nombre=${nombre}&borralog=${borralog}`);
            }
    
            // Si el nombre no existe, proceder con la inserción
            db.get("SELECT MAX(orden) AS orden FROM tipomedia", [], (err, row) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                const nuevoOrden = (row.orden || 0) + 1;
    
                // Insertar el nuevo registro en la base de datos
                db.run(
                    "INSERT INTO tipomedia (id, nombre, borralog, orden) VALUES (NULL, ?, ?, ?)",
                    [nombre, borralog, nuevoOrden],
                    (err) => {
                        if (err) {
                            console.error("Error al insertar en la base de datos:", err);
                            return res.sendStatus(500);
                        }
                        res.redirect("/admin/tipo_media/listar");
                    }
                );
            });
        });
    },
    
    show(){},
    update(req, res) {
        const idTipoMedia = req.params.idTipoMedia;
        console.log("idTipoMedia", idTipoMedia);
    
        const nombre = req.body.nombre;
        const borralog = req.body.borralog || 'valor_predeterminado';
    
        if (!nombre) {
            return res.redirect("/admin/tipo_media/edit/" + idTipoMedia + "?message=Ingrese un nombre para el Tipo Media");
        }
    
        // Verificar si ya existe un registro con el nuevo nombre
        db.get('SELECT COUNT(*) AS count FROM tipomedia WHERE nombre = ? AND id != ?', [nombre, idTipoMedia], (err, row) => {
            if (err) {
                console.error("Error al verificar la existencia de registros con el nuevo nombre:", err);
                return res.sendStatus(500);
            }
            
            if (row.count > 0) {
                const message = "El nuevo nombre ya está en uso";
                return res.redirect("/admin/tipo_media/edit/" + idTipoMedia + "?message=El nuevo nombre ya está en uso");
            }
    
            // Si el nombre no está en uso, actualizar el registro
            db.run(
                "UPDATE tipomedia SET nombre = ?, borralog = ? WHERE id = ?",
                [nombre, borralog, idTipoMedia],
                (err) => {
                    if (err) {
                        console.error("Error al actualizar la base de datos tabla Tipo Media:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/tipo_media/listar?message=Tipo Media editado exitosamente");
                }
            );
        });
    },
    
    
    edit(req, res) {
        const idTipoMedia = req.params.idTipoMedia;
        console.log("idTipoMedia", idTipoMedia);
        console.log("Datos del formulario:", req.body);
        db.get('SELECT * FROM tipomedia WHERE id = ?', [idTipoMedia], (err, tipomedia) => {
          if (err) {
            console.error("Error:", err);
            return res.sendStatus(500);
          }
      
          if (!tipomedia) {
            
            return res.status(404).render("error", { message: "Tipo de medio no encontrado" });
          }
      
          res.render("admin/tipo_media/editFormTipoMedia", {
            idTipoMedia, 
            tipomedia,
          });
        });
      },
      
    
    destroy(req,res){
        const idTipoMedia = req.params.idTipoMedia;
        console.log("idTipoMedia", idTipoMedia);
        db.get('SELECT COUNT(*) AS count FROM media WHERE nombre = ?', [idTipoMedia], (err, row) => {
            if (err) {
                console.error("Error al verificar la existencia de registros asociados en la base de datos:", err);
                return res.sendStatus(500);
            }
    
            if (row.count > 0) {
                return res.redirect("/admin/tipo_media/listar?message=No se puede eliminar el tipo Media porque existen registros asociados en la tabla media");
            }
            const nombre = req.body.nombre; 
            if (nombre){
            return res.status(400).send("El nuevo nombre del tipo de medio es obligatorio.");
            }
            
            db.run(
                "UPDATE tipomedia SET borralog = 'TRUE' WHERE id = ?",
                [idTipoMedia],
                (err) => {
                    if (err) {
                        console.error("Error al actualizar la base de datos tabla Tipo Media:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/tipo_media/listar?message=Tipo Media eliminado exitosamente");
                }
            );
        });
    }
}
module.exports = tipoMediaController;