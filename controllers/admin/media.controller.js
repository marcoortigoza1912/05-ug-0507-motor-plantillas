
const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');
const { EMPTY } = require("sqlite3");
const upload = multer({ dest: "../../public/assets/images/"});
const fileUpload = upload.single("url");//nombre de columna imagen
const fs = require("fs");



const MediaController= {
    index: async function(req,res){
        const media = await getALL(`SELECT * FROM media where borralog ='FALSE' order by orden asc`);
    
        if (media.length === 0) {
            response.status(404).render("error");
        } else {
            res.render("admin/media/index",{
                media: media,
            });
        }
    },
    create: async function(req,res){
        const mensaje = req.query.mensaje;
        try {
            const result = await getALL('SELECT matricula FROM integrantes');
            const resultTipo = await getALL('SELECT nombre FROM tipomedia');
            const matriculas = result.map(row => row.matricula);
            const nombre = resultTipo.map(row => row.nombre);
            const media = {
                titulo: req.query.titulo || '',
                matricula: req.query.matricula || '',
                borralog: req.query.borralog || '',
                nombreSeleccionado: req.query.nombre || ''
            };
           
            res.render('admin/media/formularioMedia', {
                mensaje: mensaje,
                matriculas: matriculas,
                nombre: nombre,
                media:media
            });
        } catch (error) {
            console.error('Error al obtener las matrículas:', error);
            res.status(500).render("error");
        }
    },// Crear nuevo Media

store: async function(req, res) {
  try {
    
    if (!req.body.matricula || req.body.matricula.trim() === '' ||
        !req.body.nombre || req.body.nombre.trim() === '' ||
        !req.body.titulo || req.body.titulo.trim() === '' ||
        !req.body.borralog || req.body.borralog.trim() === '') {
      return res.redirect(`/admin/media/crear?mensaje="Todos los campos son obligatorios. Favor verificar"`);
    }

   
    const mediaData = {
      nombre: req.body.nombre,
      titulo: req.body.titulo,
      borralog: req.body.borralog,
      matricula: req.body.matricula,
    };
    console.log("Contenido del formulario Media:", req.body);
   
    if (req.file) {
      const { url } = req.body; 

      const nuevoOrden = await db.get("SELECT orden FROM integrantes WHERE matricula = ?", [mediaData.matricula]);

      mediaData.url = req.file ? url + req.file.originalname : url;
      mediaData.orden = nuevoOrden ? nuevoOrden.orden : 0;

      
      await db.run(
        "INSERT INTO media (id, nombre, src, url, titulo, borralog, matricula, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        [null, mediaData.nombre, null, mediaData.url, mediaData.titulo, mediaData.borralog, mediaData.matricula, mediaData.orden]
      );
    } else if (req.body.texto) {
      mediaData.url = req.body.texto; 
      const nuevoOrden = await db.get("SELECT orden FROM integrantes WHERE matricula = ?", [mediaData.matricula]);

      mediaData.orden = nuevoOrden ? nuevoOrden.orden : 0;

      await db.run(
        "INSERT INTO media (id, nombre, src, url, titulo, borralog, matricula, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        [null, mediaData.nombre, null, mediaData.url, mediaData.titulo, mediaData.borralog, mediaData.matricula, mediaData.orden]
      );
    }

    
    res.redirect("/admin/media/listar?success=" + encodeURIComponent("¡Registro insertado correctamente!"));

  } catch (error) {
    console.error("Error:", error);
    res.sendStatus(500);
  }
},

    show(){},
    update: async function(req, res) {
        const idMedia = req.params.idMedia;
    
        try {
            
            const media = await db.get("SELECT * FROM media WHERE id = ?", [idMedia]);
    
            if (!media) {
                return res.redirect(`/admin/media/listar?error=` + encodeURIComponent("¡El registro no existe!"));
            }
    
            
            if (!req.body.titulo || req.body.titulo.trim() === '' ||
                !req.body.borralog || req.body.borralog.trim() === '') {
                return res.redirect(`/admin/media/edit/${idMedia}?mensaje=` + encodeURIComponent("¡Los campos Título y Estado son obligatorios!"));
            }
    
            
            let updateData = {
                titulo: req.body.titulo,
                borralog: req.body.borralog,
            };
    
            if (req.file) {
                const { nombre, url } = req.body; 
                const nuevoOrden = await db.get("SELECT orden FROM integrantes WHERE matricula = ?", [media.matricula]); // Use existing matricula
    
                updateData.url = req.file ? url + req.file.originalname : url; 
                updateData.orden = nuevoOrden ? nuevoOrden.orden : 0;
    
            } else if (req.body.texto) {
                updateData.url = req.body.texto;
            }
    
            
            await db.run(
                "UPDATE media SET url = ?, titulo = ?, borralog = ?, orden = ? WHERE id = ?",
                [updateData.url, updateData.titulo, updateData.borralog, updateData.orden, idMedia]
            );
    
            
            return res.redirect("/admin/media/listar?success=" + encodeURIComponent("¡Registro actualizado correctamente!"));
    
        } catch (error) {
            console.error("Error:", error);
            res.sendStatus(500);
        }
    },
    
    edit(req,res){
        const idMedia = req.params.idMedia;
        console.log("idMedia", idMedia);
        
        db.get('SELECT * FROM media WHERE id = ?', [idMedia], (err, media) => {
            if (media.length === 0) {
                response.status(404).render("error");
            } else {
                console.log("Datos del medio:", media);
                res.render("admin/media/editFormMedia",{
                idMedia: idMedia,
                media: media,
                });
            }
        });
    },
    destroy(req,res){
        const idMedia = req.params.idMedia;        
        db.run(
            "UPDATE media SET borralog = 'TRUE' WHERE id = ?",
            [idMedia],
            (err) => {
                if (err) {
                    console.error("Error al actualizar la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/media/listar?message=Media eliminado exitosamente");
            }
        );            
    }
};

module.exports = MediaController;