const { getALL, db} = require ("../../db/conexion");

const IntegrantesController = {
    index:  async function(req, res){
        const integrante =  await  getALL(`SELECT * FROM integrantes where borralog ='FALSE' order by orden asc`);
        if (integrante.length === 0) {
            response.status(404).render("error");
        } else {
            res.render("admin/integrantes/index",{
            integrantes: integrante,
            });
        }
    },
    create: async function(req, res) {
        const mensaje = req.query.mensaje;
        const integrante = {
            matricula: req.query.matricula || '',
            nombre: req.query.nombre || '',
            apellido: req.query.apellido || '',
            borralog: req.query.borralog || ''
        };
        res.render('admin/integrantes/crearFormulario', {
            mensaje: mensaje,
            integrante: integrante
        });
    },
    
    store: async function(req,res){
        const { matricula, nombre, apellido, borralog } = req.body;
    if (!req.body.matricula || req.body.matricula.trim() === ''
        || !req.body.nombre || req.body.nombre.trim() === ''
        || !req.body.apellido || req.body.apellido.trim() === ''
        || !req.body.borralog || req.body.borralog.trim() === '')
        return res.redirect(`/admin/integrantes/crear?mensaje="Todos los campos son obligatorios. Favor verificar"&matricula=${matricula}&nombre=${nombre}&apellido${apellido}&borralog=${borralog}`
        );
    console.log("contenido del formulario", req.body);
    
    db.get("SELECT MAX(orden) as orden FROM integrantes", [], (err, row) => {
        if (err) {
            console.error("Error al obtener el máximo orden:", err);
            return res.sendStatus(500);
        }
            const nuevoOrden = (row.orden || 0)+1;
            db.run(
                "INSERT INTO integrantes (matricula, nombre, apellido, borralog, orden) VALUES (?, ?, ?, ?, ?)",
                [matricula, nombre, apellido, borralog, nuevoOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/integrantes/listar");
                }
            );    
    });
    },

    show(){},
    update(req, res) {
        const idIntegrante = req.params.idIntegrante;
        console.log("idIntegrante", idIntegrante);
        console.log("contenido del formulario", req.body);
        //validacion de datos
        const { matricula, nombre, apellido, borralog } = req.body;
        if (!req.body.nombre || req.body.nombre.trim() === ''
            || !req.body.apellido || req.body.apellido.trim() === ''
            || !req.body.borralog || req.body.borralog.trim() === '')
            return res.redirect(`/admin/integrantes/edit/${idIntegrante}?mensaje="Todos los campos son obligatorios. Favor verificar"&matricula=${matricula}&nombre=${nombre}&apellido${apellido}&borralog=${borralog}`
            );
        db.run(
            `UPDATE integrantes SET 
            nombre = ?, apellido = ?, borralog = ?
            WHERE matricula = ?`,
            [req.body.nombre, req.body.apellido, req.body.borralog, idIntegrante], // Cambio matricula por idIntegrante
            (err) => {
                if (err) {
                    console.error("Error al actualizar la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/integrantes/listar?message=Integrante actualizado exitosamente")
            }
        );
    },
    
    edit(req, res) {
        const idIntegrante = req.params.idIntegrante;
        console.log("idIntegrante", idIntegrante);
        
        db.get('SELECT * FROM integrantes WHERE matricula = ?', [idIntegrante], (err, integrante) => {
            if (err) {
                console.error("Error:",err);
                return res.sendStatus(500);
            } 
            if (!integrante) {
                // Si no se encontró ningún integrante con el ID dado
                return res.status(404).render("error");
            } 
            
            res.render("admin/integrantes/editFormIntegrante",{
            idIntegrante: idIntegrante,
            integrante: integrante,
            });
            
        });
    },
    
    /***************************** */
    destroy(req, res) {
        const idIntegrante = req.params.idIntegrante;
    
        db.get("SELECT COUNT(*) AS count FROM media WHERE matricula = ? AND borralog = 'FALSE'", [idIntegrante], (err, row) => {
            if (err) {
                console.error("Error al verificar la existencia de registros asociados en la base de datos:", err);
                return res.sendStatus(500);
            }
    
            if (row.count > 0) {
                
                return res.redirect("/admin/integrantes/listar?message=No se puede eliminar el integrante porque existen registros asociados en la tabla media");
            }
    
            
            db.run(
                "UPDATE integrantes SET borralog = 'TRUE' WHERE matricula = ?",
                [idIntegrante],
                (err) => {
                    if (err) {
                        console.error("Error al actualizar la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/integrantes/listar?message=Integrante eliminado exitosamente");
                }
            );
        });
        
    }
    
};


module.exports = IntegrantesController;