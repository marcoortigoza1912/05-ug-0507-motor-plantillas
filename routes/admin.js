const express = require('express');
const router = express.Router();
const { getALL, db} = require ("../db/conexion");
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: "./public/assets/images"});
const fileUpload = upload.single("url");//nombre de columna imagen
//const db = require("../db/data")
const sqlite3 = require('sqlite3');
const { error } = require('console');
const session = require('express-session');

router.use(session({
    secret: 'tu_secreto_aqui', // Cambia esto por una cadena secreta segura
    resave: false,
    saveUninitialized: false
}));
router.get('/', (req, res) =>{
    res.render("admin/index")
});


const AdminIntegrantesController = require('../controllers/admin/integrantes.controller');
const MediaController = require('../controllers/admin/media.controller');
const tipoMediaController = require('../controllers/admin/tipoMedia.controller');
// INTEGRANTES
router.get('/integrantes/listar', AdminIntegrantesController.index);
router.get('/integrantes/crear',AdminIntegrantesController.create);
router.post('/integrantes/create', AdminIntegrantesController.store);

router.post('/integrantes/delete/:idIntegrante', AdminIntegrantesController.destroy);
router.get('/integrantes/edit/:idIntegrante', AdminIntegrantesController.edit);
router.post('/integrantes/update/:idIntegrante', AdminIntegrantesController.update);
// Media
router.get('/media/listar', MediaController.index);
router.get('/media/crear', MediaController.create);
router.post('/media/create', fileUpload, MediaController.store);

router.post('/media/delete/:idMedia', MediaController.destroy);
router.get('/media/edit/:idMedia', MediaController.edit);
router.post('/media/update/:idMedia', fileUpload,MediaController.update);
//Tipo - Media
router.get('/tipo_media/listar', tipoMediaController.index);
router.get('/tipo_media/crear', tipoMediaController.create);
router.post('/tipo_media/create', tipoMediaController.store);

router.post('/tipo_media/delete/:idTipoMedia', tipoMediaController.destroy);
router.get('/tipo_media/edit/:idTipoMedia', tipoMediaController.edit);
router.post('/tipo_media/update/:idTipoMedia', tipoMediaController.update);
                                                
module.exports = router;




